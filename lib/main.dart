
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:untitled/pages/Home.dart';
import 'package:untitled/pages/MainPage.dart';
import 'package:untitled/pages/PigDog.dart';
import 'package:untitled/pages/Settings.dart';
import 'package:untitled/pages/Info.dart';

void main() => runApp(
    ChangeNotifierProvider(create: (context) => Settings(),
      child: MaterialApp(
        theme: ThemeData(
          primaryColor: Colors.blue,
        ),
        initialRoute: '/',
        routes: {
          '/' : (context) => MainPage(),
          '/settings' : (context) => Settings(),
          '/pigdog' : (context) => PigDog(),
          '/info' : (context) => Info(),
        },
      ),
    )
);