import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class Info extends StatefulWidget {
  const Info({Key? key}) : super(key: key);

  @override
  State<Info> createState() => _SettingsState();
}

class _SettingsState extends State<Info> {

  late NetworkImage bgImage;
  late double width;
  late double height;
  late double fontsize;

  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(builder: (context, orientation)
    {
      if (MediaQuery
          .of(context)
          .orientation == Orientation.landscape) {
        bgImage = NetworkImage("https://wallpapercave.com/wp/wp8801330.jpg");
        width = 250;
        height = 30;
        fontsize = 1;
      }
      else {
        bgImage = NetworkImage("https://cdn.wallpapersafari.com/70/85/lEKwXJ.jpg");
        width = MediaQuery.of(context).size.width - 150;
        height = 50;
        fontsize = 2;
      }
      return Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: bgImage,
                fit: BoxFit.cover
            ),
          ),
          child:
          Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                BackdropFilter(
                  filter: ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white.withOpacity(0.0)),
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Padding(padding: const EdgeInsets.only(top: 10)),
                    Container(
                      margin: const EdgeInsets.all(10),
                      height: height,
                      width: width,
                      decoration: BoxDecoration(
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                            color: Colors.blue.withOpacity(0.1),
                            blurRadius: 1,
                            offset: const Offset(10, 10),
                          ),
                        ],
                      ),
                      child: RaisedButton(
                        elevation: 30,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(0.0),
                            side: const BorderSide(
                                color: const Color.fromRGBO(0, 160, 227, 1))),
                        onPressed: () {
                          launch("https://savelife.in.ua/");
                        },
                        padding: const EdgeInsets.all(10.0),
                        color: const Color.fromRGBO(0, 160, 227, 1),
                        textColor: Colors.white,
                        child: Text("Here you can donate".toUpperCase(),
                            style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(fontsize)),
                        ),
                      )
                    ),
                    Container(
                      margin: const EdgeInsets.all(10),
                      height: height,
                      width: width,
                      decoration: BoxDecoration(
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                            color: Colors.blue.withOpacity(0.1),
                            blurRadius: 1,
                            offset: const Offset(10, 10),
                          ),
                        ],
                      ),
                      child: RaisedButton(
                        elevation: 30,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(0.0),
                            side: const BorderSide(
                                color: const Color.fromRGBO(0, 160, 227, 1))),
                        onPressed: () {
                          launch("https://deepstatemap.live/");
                        },
                        padding: const EdgeInsets.all(10.0),
                        color: const Color.fromRGBO(0, 160, 227, 1),
                        textColor: Colors.white,
                        child: Text("There is a war map".toUpperCase(),
                            style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(fontsize))),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.all(10),
                      height: height,
                      width: width,
                      decoration: BoxDecoration(
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                            color: Colors.blue.withOpacity(0.1),
                            blurRadius: 1,
                            offset: const Offset(10, 10),
                          ),
                        ],
                      ),
                      child: RaisedButton(
                        elevation: 30,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(0.0),
                            side: const BorderSide(
                                color: const Color.fromRGBO(0, 160, 227, 1))),
                        onPressed: () {
                          launch("https://www.donor.ua/centers/");
                        },
                        padding: const EdgeInsets.all(10.0),
                        color: const Color.fromRGBO(0, 160, 227, 1),
                        textColor: Colors.white,
                        child: Text("Become a donor".toUpperCase(),
                            style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(fontsize)),
                        ),
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(bottom: 40)),
                    const Text(
                      "P.s. Цей додаток був створений з метою отримання максимального балу",
                      style: TextStyle(
                        fontSize: 9,
                        fontWeight: FontWeight.bold,
                        fontFamily: "Times New Roman",
                      ),
                    )
                  ],
                ),
              ],
            ),
          )
      );
    });
  }
}