// ignore_for_file: prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Settings extends StatefulWidget with ChangeNotifier {
  Settings({Key? key}) : super(key: key);

  double _maximumHealth = 100;
  int _healingSpeed = 1;
  double _health = 100;

  double get maximumHealth => _maximumHealth;
  set maximumHealth(double maximumHealth) {
    _maximumHealth = maximumHealth;
    _health = maximumHealth;
    notifyListeners(); //Notifies its listeners that the value has changed
  }

  int get healingSpeed => _healingSpeed;
  set healingSpeed(int healingSpeed) {
    _healingSpeed = healingSpeed;
    notifyListeners(); //Notifies its listeners that the value has changed
  }

  double get health => _health;
  set health(double health) {
    _health = health;
    notifyListeners(); //Notifies its listeners that the value has changed
  }

  @override
  State<Settings> createState() => _SettingsState();
}

class _SettingsState extends State<Settings>{

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                children: [
                  Text("Here you can set difficulty",
                    style: TextStyle(
                      fontSize: 25
                    ),
                  ),
                ],
              ),
              Padding(padding: EdgeInsets.only(top: 20)),
              Row(
                children: [
                  Text("Maximum health: ",
                    style: TextStyle(
                        fontSize: 15
                    ),
                  ),
                  Slider(
                      value: Provider.of<Settings>(context, listen: false).maximumHealth,
                      min: 10,
                      max: 200,
                      divisions: 19,
                      label: Provider.of<Settings>(context, listen: false).maximumHealth.round().toString(),
                      onChanged: (newValue) { setState(() => {
                        Provider.of<Settings>(context, listen: false).maximumHealth = newValue.round().toDouble()
                      });
                      }),
                ],
              ),
              Row(
                children: [
                  Text("Regeneration speed: ",
                    style: TextStyle(
                        fontSize: 15
                    ),
                  ),
                  Slider(
                      value: Provider.of<Settings>(context, listen: false).healingSpeed.toDouble(),
                      max: 5,
                      divisions: 5,
                      label: Provider.of<Settings>(context, listen: false).healingSpeed.toString(),
                      onChanged: (newValue) { setState(() => {
                        Provider.of<Settings>(context, listen: false).healingSpeed = newValue.round()
                      });
                      }),
                ],
              ),
            ],
          ),
        ],
      )
    );
  }
}
