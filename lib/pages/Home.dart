// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'dart:io';
import 'dart:ui';
import 'package:flutter/services.dart';

import 'package:flutter/material.dart';
import 'package:untitled/pages/Info.dart';
import 'package:untitled/pages/PigDog.dart';
import 'package:untitled/pages/Settings.dart';

class Home extends StatefulWidget {
  Home({required this.onChanged});
  final ValueChanged<int> onChanged;
  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {

  late NetworkImage bgImage;

  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(builder: (context, orientation)
    {
      if (MediaQuery
          .of(context)
          .orientation == Orientation.landscape) {
        bgImage = new NetworkImage("https://wallpapercave.com/wp/wp8801330.jpg");
      }
      else {
        bgImage = new NetworkImage("https://cdn.wallpapersafari.com/70/85/lEKwXJ.jpg");
      }
      return Container(
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: bgImage,
              fit: BoxFit.cover
          ),
        ),
        child: SafeArea(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.0)),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(padding: EdgeInsets.only(top: 30)),
                  Container(
                    color: Colors.grey.withOpacity(0.5),
                    child: Text(
                      "Welcome to my app :)\nPress 'Kill' to start killing pigdogs",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontFamily: "Times New Roman",
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(top: 30)),
                  OrientationBuilder(builder: (context, orientation) {
                    if (MediaQuery
                        .of(context)
                        .orientation == Orientation.landscape) {
                      return SizedBox(
                        width: 200,
                        height: 50,
                        child: OutlinedButton.icon(
                          onPressed: () {
                            // pageController.animateToPage('/pigdog', duration: Duration(milliseconds: 500), curve: Curves.bounceIn);
                            // TODO
                            widget.onChanged(1);
                          },
                          label: Text(
                            "Kill",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 30,
                              fontFamily: "Times New Roman",
                            ),
                          ),
                          icon: Image.asset(
                            "assets/knife-icon.png", fit: BoxFit.cover,
                            width: 50,),
                        ),
                      );
                    }
                    else {
                      return SizedBox(
                        width: 200,
                        height: 100,

                        child: OutlinedButton.icon(
                          onPressed: () {
                            widget.onChanged(1);
                          },
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(Colors.grey.withOpacity(0.5)),
                          ),
                          label: Text(
                            "Kill",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 50,
                              fontFamily: "Times New Roman",
                            ),
                          ),
                          icon: Image.asset(
                            "assets/knife-icon.png", fit: BoxFit.cover,
                            width: 70,
                          ),
                        ),
                      );
                    }
                  }
                  ),
                  Padding(padding: EdgeInsets.only(top: 30)),
                  OutlinedButton.icon(onPressed: () {
                    exit(0);
                  }, icon: Icon(Icons.close), label: Text("Close App"))
                ],
              ),
            ],
          ),
        ),
      );
    });
  }
}
