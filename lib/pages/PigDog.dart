// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import 'Settings.dart';


class PigDog extends StatefulWidget {
  const PigDog({Key? key}) : super(key: key);


  @override
  State<PigDog> createState() => _PigDogState();
}

class _PigDogState extends State<PigDog> {

  AssetImage bgImage = new AssetImage("assets/pigdog.jpg");

  late Timer timer;

  @override
  initState()
  {
    timer = createTimer();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<Settings>(
        builder: (context, model, child)
      {
        return GestureDetector(
          child: Container(

            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: bgImage,
                  fit: BoxFit.cover
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(padding: EdgeInsets.only(bottom: 10),
                  child: Text(
                    "Health : ${model.health}",
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
                Padding(padding: EdgeInsets.only(bottom: 30),
                  child: SizedBox(
                    width: MediaQuery
                        .of(context)
                        .size
                        .width - 100,
                    height: 20,
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      child: LinearProgressIndicator(
                        backgroundColor: Colors.grey.withOpacity(0.5),
                        valueColor: AlwaysStoppedAnimation(Colors.red),
                        minHeight: 25,
                        value: model.health / model.maximumHealth,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          onTap: () {
            setState(() {
              if (model.health == 0)
              {
                refreshGame();
                return;
              }
              if (model.health - 2 <= 0) {
                bgImage = new AssetImage('assets/dead-pigdog.jpg');
                timer.cancel();
                model.health = 0;
                _showMyDialog();
              }
              else if (model.health - 2 > 0) {
                model.health -= 2;
              }
            });
          },
        );
      }
    );
  }
  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () async {
            return false;
          },
          child: AlertDialog(
            title: const Text('PigDog killed'),
            content: Container(
                height: 300,
                child:Text('While you were playing - ЗСУ killed at least 5 real invaders.\n'
                      'Instead of wasting time - you can do smth useful.\n'
                      'For example - you can donate on ЗСУ :)\n'
                      'Don`t be shy. And thanks!'),
              ),
            actions: <Widget>[
              TextButton(
                child: const Text('Donate'),
                onPressed: () async {
                  Navigator.of(context).pop();
                  await launch("https://savelife.in.ua/");
                },
              ),
            ],
          ),
        );
      },
    );
  }

  Timer createTimer()
  {
    return Timer.periodic(Duration(seconds: 1), (Timer t) => {
      if (Provider.of<Settings>(context, listen: false).health < Provider.of<Settings>(context, listen: false).maximumHealth)
        {
          Provider.of<Settings>(context, listen: false).health += Provider.of<Settings>(context, listen: false).healingSpeed
        }
    });
  }

  void refreshGame()
  {
    timer = createTimer();
    Provider.of<Settings>(context, listen: false).health = Provider.of<Settings>(context, listen: false).maximumHealth;
    bgImage = new AssetImage("assets/pigdog.jpg");
  }
}
